<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230201210005 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE address (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', registration_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', adult_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', profession_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', street VARCHAR(255) NOT NULL, number INT NOT NULL, city VARCHAR(255) NOT NULL, county VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_D4E6F81833D8F43 (registration_id), INDEX IDX_D4E6F817CEA3A6D (adult_id), INDEX IDX_D4E6F81FDEF8996 (profession_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adult (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, gender VARCHAR(255) NOT NULL, sexual_orientation VARCHAR(255) NOT NULL, financial_bracket VARCHAR(255) NOT NULL, skin_tone VARCHAR(255) NOT NULL, food_preference VARCHAR(255) NOT NULL, age INT NOT NULL, eye_color VARCHAR(255) NOT NULL, hair_color VARCHAR(255) NOT NULL, music_preference VARCHAR(255) NOT NULL, activity_level VARCHAR(255) NOT NULL, marital_status VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_1A2EF281A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adult_education (adult_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', education_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_803C5087CEA3A6D (adult_id), INDEX IDX_803C5082CA1BD71 (education_id), PRIMARY KEY(adult_id, education_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adult_hobby (adult_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', hobby_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_51825407CEA3A6D (adult_id), INDEX IDX_5182540322B2123 (hobby_id), PRIMARY KEY(adult_id, hobby_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adult_profession (adult_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', profession_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_DC463F387CEA3A6D (adult_id), INDEX IDX_DC463F38FDEF8996 (profession_id), PRIMARY KEY(adult_id, profession_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE adult_address (adult_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', address_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_280CB2D7CEA3A6D (adult_id), INDEX IDX_280CB2DF5B7AF75 (address_id), PRIMARY KEY(adult_id, address_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE breed (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, fur_color VARCHAR(255) NOT NULL, fur_length VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE child (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', adult_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', user_id BINARY(16) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', temperament VARCHAR(255) NOT NULL, has_siblings TINYINT(1) NOT NULL, height INT NOT NULL, weight INT NOT NULL, style_of_attachment VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, gender VARCHAR(255) NOT NULL, sexual_orientation VARCHAR(255) NOT NULL, financial_bracket VARCHAR(255) NOT NULL, skin_tone VARCHAR(255) NOT NULL, food_preference VARCHAR(255) NOT NULL, age INT NOT NULL, eye_color VARCHAR(255) NOT NULL, hair_color VARCHAR(255) NOT NULL, music_preference VARCHAR(255) NOT NULL, activity_level VARCHAR(255) NOT NULL, marital_status VARCHAR(255) NOT NULL, INDEX IDX_22B354297CEA3A6D (adult_id), UNIQUE INDEX UNIQ_22B35429A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE child_education (child_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', education_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', INDEX IDX_A3B3E5EDD62C21B (child_id), INDEX IDX_A3B3E5E2CA1BD71 (education_id), PRIMARY KEY(child_id, education_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE dog (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', owner_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', breed_id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, behaviour VARCHAR(255) NOT NULL, INDEX IDX_812C397D7E3C61F9 (owner_id), INDEX IDX_812C397DA8B4A30F (breed_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE education (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, level VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hobby (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, requires_investing TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE profession (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, financial_bracket VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE registration (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', internal_id VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id BINARY(16) NOT NULL COMMENT \'(DC2Type:uuid)\', active TINYINT(1) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, roles JSON NOT NULL, token VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F81833D8F43 FOREIGN KEY (registration_id) REFERENCES registration (id)');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F817CEA3A6D FOREIGN KEY (adult_id) REFERENCES adult (id)');
        $this->addSql('ALTER TABLE address ADD CONSTRAINT FK_D4E6F81FDEF8996 FOREIGN KEY (profession_id) REFERENCES profession (id)');
        $this->addSql('ALTER TABLE adult ADD CONSTRAINT FK_1A2EF281A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE adult_education ADD CONSTRAINT FK_803C5087CEA3A6D FOREIGN KEY (adult_id) REFERENCES adult (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adult_education ADD CONSTRAINT FK_803C5082CA1BD71 FOREIGN KEY (education_id) REFERENCES education (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adult_hobby ADD CONSTRAINT FK_51825407CEA3A6D FOREIGN KEY (adult_id) REFERENCES adult (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adult_hobby ADD CONSTRAINT FK_5182540322B2123 FOREIGN KEY (hobby_id) REFERENCES hobby (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adult_profession ADD CONSTRAINT FK_DC463F387CEA3A6D FOREIGN KEY (adult_id) REFERENCES adult (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adult_profession ADD CONSTRAINT FK_DC463F38FDEF8996 FOREIGN KEY (profession_id) REFERENCES profession (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adult_address ADD CONSTRAINT FK_280CB2D7CEA3A6D FOREIGN KEY (adult_id) REFERENCES adult (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE adult_address ADD CONSTRAINT FK_280CB2DF5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE child ADD CONSTRAINT FK_22B354297CEA3A6D FOREIGN KEY (adult_id) REFERENCES adult (id)');
        $this->addSql('ALTER TABLE child ADD CONSTRAINT FK_22B35429A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE child_education ADD CONSTRAINT FK_A3B3E5EDD62C21B FOREIGN KEY (child_id) REFERENCES child (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE child_education ADD CONSTRAINT FK_A3B3E5E2CA1BD71 FOREIGN KEY (education_id) REFERENCES education (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE dog ADD CONSTRAINT FK_812C397D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES adult (id)');
        $this->addSql('ALTER TABLE dog ADD CONSTRAINT FK_812C397DA8B4A30F FOREIGN KEY (breed_id) REFERENCES breed (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F81833D8F43');
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F817CEA3A6D');
        $this->addSql('ALTER TABLE address DROP FOREIGN KEY FK_D4E6F81FDEF8996');
        $this->addSql('ALTER TABLE adult DROP FOREIGN KEY FK_1A2EF281A76ED395');
        $this->addSql('ALTER TABLE adult_education DROP FOREIGN KEY FK_803C5087CEA3A6D');
        $this->addSql('ALTER TABLE adult_education DROP FOREIGN KEY FK_803C5082CA1BD71');
        $this->addSql('ALTER TABLE adult_hobby DROP FOREIGN KEY FK_51825407CEA3A6D');
        $this->addSql('ALTER TABLE adult_hobby DROP FOREIGN KEY FK_5182540322B2123');
        $this->addSql('ALTER TABLE adult_profession DROP FOREIGN KEY FK_DC463F387CEA3A6D');
        $this->addSql('ALTER TABLE adult_profession DROP FOREIGN KEY FK_DC463F38FDEF8996');
        $this->addSql('ALTER TABLE adult_address DROP FOREIGN KEY FK_280CB2D7CEA3A6D');
        $this->addSql('ALTER TABLE adult_address DROP FOREIGN KEY FK_280CB2DF5B7AF75');
        $this->addSql('ALTER TABLE child DROP FOREIGN KEY FK_22B354297CEA3A6D');
        $this->addSql('ALTER TABLE child DROP FOREIGN KEY FK_22B35429A76ED395');
        $this->addSql('ALTER TABLE child_education DROP FOREIGN KEY FK_A3B3E5EDD62C21B');
        $this->addSql('ALTER TABLE child_education DROP FOREIGN KEY FK_A3B3E5E2CA1BD71');
        $this->addSql('ALTER TABLE dog DROP FOREIGN KEY FK_812C397D7E3C61F9');
        $this->addSql('ALTER TABLE dog DROP FOREIGN KEY FK_812C397DA8B4A30F');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE adult');
        $this->addSql('DROP TABLE adult_education');
        $this->addSql('DROP TABLE adult_hobby');
        $this->addSql('DROP TABLE adult_profession');
        $this->addSql('DROP TABLE adult_address');
        $this->addSql('DROP TABLE breed');
        $this->addSql('DROP TABLE child');
        $this->addSql('DROP TABLE child_education');
        $this->addSql('DROP TABLE dog');
        $this->addSql('DROP TABLE education');
        $this->addSql('DROP TABLE hobby');
        $this->addSql('DROP TABLE profession');
        $this->addSql('DROP TABLE registration');
        $this->addSql('DROP TABLE user');
    }
}
