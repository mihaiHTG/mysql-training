<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class DummyTest extends TestCase
{
    public function testExample(): void
    {
        static::assertEmpty('');
    }
}
