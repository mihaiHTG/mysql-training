<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Adult;
use App\Entity\Breed;
use App\Entity\Child;
use App\Entity\Dog;
use App\Entity\Education;
use App\Entity\Hobby;
use App\Entity\Profession;
use App\Entity\Registration;
use App\Entity\User;
use App\Model\List\DogBreedList;
use App\Model\List\PersonFeaturesList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
final class AppFixtures extends Fixture
{
    private const NUMBER_OF_ENTRIES = 500;
    private const MAX_NR_CHILDREN = 20;
    private const MAX_NR_HOBBIES = 20;
    private const MAX_NR_H_EDUCATION = 20;
    private const MAX_NR_L_EDUCATION = 20;
    private const MAX_NR_PROFESSIONS = 20;
    private const MAX_NR_P_ADDRESSES = 20;
    private const MAX_NR_A_ADDRESSES = 20;
    private const MAX_NR_DOGS = 20;

    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        for ($index = 0; $index <= self::NUMBER_OF_ENTRIES; ++$index) {
            $adult = $this->createAdult();

            if (1 === random_int(0, 1)) {
                $user = $this->createUser();
                $adult->user = $user;
                $manager->persist($user);
            }

            $maxNrChildren = random_int(0, self::MAX_NR_CHILDREN);
            for ($childIndex = 0; $childIndex <= $maxNrChildren; ++$childIndex) {
                $child = $this->createChild();
                $adult->children[] = $child;
                $child->adult = $adult;
                if ($maxNrChildren >= 2) {
                    $child->hasSiblings = true;
                }

                $maxLEducation = random_int(0, self::MAX_NR_L_EDUCATION);
                for ($lEducationIndex = 0; $lEducationIndex <= $maxLEducation; ++$lEducationIndex) {
                    $lEducation = $this->createLowerEducation();
                    $child->educations[] = $lEducation;
                    $manager->persist($lEducation);
                }

                $manager->persist($child);
            }

            $maxHEducation = random_int(0, self::MAX_NR_H_EDUCATION);
            for ($hEducationIndex = 0; $hEducationIndex <= $maxHEducation; ++$hEducationIndex) {
                $hEducation = $this->createHigherEducation();
                $adult->educations[] = $hEducation;
                $manager->persist($hEducation);
            }

            $maxHobbies = random_int(0, self::MAX_NR_HOBBIES);
            for ($hobbyIndex = 0; $hobbyIndex <= $maxHobbies; ++$hobbyIndex) {
                $hobby = $this->createHobby();
                $adult->hobbies[] = $hobby;
                $manager->persist($hobby);
            }

            $maxAAddress = random_int(0, self::MAX_NR_A_ADDRESSES);
            for ($aAddressIndex = 0; $aAddressIndex <= $maxAAddress; ++$aAddressIndex) {
                $aAddress = $this->createAddress();
                $aRegistration = $this->createRegistration();
                $aAddress->registration = $aRegistration;
                $manager->persist($aRegistration);
                $adult->addresses[] = $aAddress;
                $manager->persist($aAddress);
            }

            $maxProfessions = random_int(0, self::MAX_NR_PROFESSIONS);
            for ($professionsIndex = 0; $professionsIndex <= $maxProfessions; ++$professionsIndex) {
                $profession = $this->createProfession();

                $maxPAddress = random_int(0, self::MAX_NR_P_ADDRESSES);
                for ($pAddressIndex = 0; $pAddressIndex <= $maxPAddress; ++$pAddressIndex) {
                    $pAddress = $this->createAddress();
                    $pRegistration = $this->createRegistration();
                    $pAddress->registration = $pRegistration;
                    $manager->persist($pRegistration);
                    $profession->addresses[] = $pAddress;
                    $manager->persist($pAddress);
                }
                $adult->professions[] = $profession;
                $manager->persist($profession);
            }

            $maxDogs = random_int(0, self::MAX_NR_DOGS);
            for ($dogIndex = 0; $dogIndex <= $maxDogs; ++$dogIndex) {
                $dog = $this->createDog();
                $breed = $this->createBreed();
                $dog->breed = $breed;
                $dog->owner = $adult;
                $manager->persist($breed);
                $adult->dogs[] = $dog;
                $manager->persist($dog);
            }
            $manager->persist($adult);
            dump(sprintf('Index %d of %d', $index, self::NUMBER_OF_ENTRIES));
        }
        dump(sprintf('Index %d of %d', self::NUMBER_OF_ENTRIES, self::NUMBER_OF_ENTRIES));
        dump('Flushing!');
        $manager->flush();
        dump('Done!');
    }

    /**
     * @throws \Exception
     */
    private function createHobby(): Hobby
    {
        $hobby = new Hobby();

        $features = PersonFeaturesList::HOBBIES[random_int(0, \count(PersonFeaturesList::HOBBIES) - 1)];
        $features = explode(' | ', $features);

        $hobby->name = $features[0];
        $hobby->requiresInvesting = (bool) $features[1];

        return $hobby;
    }

    /**
     * @throws \Exception
     */
    private function createHigherEducation(): Education
    {
        $education = new Education();
        $education->name = PersonFeaturesList::UNIVERSITIES[random_int(
            0,
            \count(PersonFeaturesList::UNIVERSITIES) - 1
        )];
        $education->level = PersonFeaturesList::UNIVERSITY_LEVEL[random_int(
            0,
            \count(PersonFeaturesList::UNIVERSITY_LEVEL) - 1
        )];

        return $education;
    }

    /**
     * @throws \Exception
     */
    private function createLowerEducation(): Education
    {
        $school = new Education();
        $school->name = PersonFeaturesList::SCHOOLS[random_int(0, \count(PersonFeaturesList::SCHOOLS) - 1)];
        $school->level = PersonFeaturesList::SCHOOL_LEVELS[random_int(
            0,
            \count(PersonFeaturesList::SCHOOL_LEVELS) - 1
        )];

        return $school;
    }

    /**
     * @throws \Exception
     */
    private function createProfession(): Profession
    {
        $profession = new Profession();
        $features = PersonFeaturesList::PROFESSIONS[random_int(0, \count(PersonFeaturesList::PROFESSIONS) - 1)];
        $features = explode(' | ', $features);

        $profession->name = $features[0];
        $profession->financialBracket = $features[1];

        return $profession;
    }

    /**
     * @throws \Exception
     */
    private function createAddress(): Address
    {
        $address = new Address();
        $address->street = PersonFeaturesList::STREET_NAMES[random_int(
            0,
            \count(PersonFeaturesList::STREET_NAMES) - 1
        )];
        $address->number = random_int(1, 500);
        $cityState = PersonFeaturesList::CITY_STATE[random_int(0, \count(PersonFeaturesList::CITY_STATE) - 1)];
        $cityState = explode(', ', $cityState);
        $address->city = $cityState[0];
        $address->county = $cityState[1];
        $address->country = 'US';

        return $address;
    }

    /**
     * @throws \Exception
     */
    private function createRegistration(): Registration
    {
        $registration = new Registration();
        $registration->internalId = bin2hex(random_bytes(10));

        return $registration;
    }

    /**
     * @throws \Exception
     */
    private function createUser(): User
    {
        $user = new User();
        $user->active = true;
        $user->email = sprintf('%s.example.com', bin2hex(random_bytes(10)));
        $user->plainPassword = 'q1w2e3r4';
        $user->roles = [User::ROLE_USER];

        return $user;
    }

    /**
     * @throws \Exception
     */
    private function createBreed(): Breed
    {
        $breed = new Breed();
        $features = DogBreedList::BREED_LIST_UNEXPANDED[random_int(0, \count(DogBreedList::BREED_LIST_UNEXPANDED) - 1)];
        $features = explode(' | ', $features);

        $breed->name = $features[0];
        $breed->furColor = $features[1];
        $breed->furLength = $features[2];

        return $breed;
    }

    /**
     * @throws \Exception
     */
    private function createDog(): Dog
    {
        $dog = new Dog();
        $dog->firstName = DogBreedList::DOG_NAME_LIST[random_int(0, \count(DogBreedList::DOG_NAME_LIST) - 1)];
        $dog->behaviour = DogBreedList::BEHAVIOURS[random_int(0, \count(DogBreedList::BEHAVIOURS) - 1)];

        return $dog;
    }

    /**
     * @throws \Exception
     */
    private function createAdult(): Adult
    {
        $adult = new Adult();
        $adult->firstName = PersonFeaturesList::FIRST_NAMES[random_int(0, \count(PersonFeaturesList::FIRST_NAMES) - 1)];
        $adult->lastName = PersonFeaturesList::LAST_NAMES[random_int(0, \count(PersonFeaturesList::LAST_NAMES) - 1)];
        $adult->gender = PersonFeaturesList::GENDERS[random_int(0, \count(PersonFeaturesList::GENDERS) - 1)];
        $adult->sexualOrientation = PersonFeaturesList::ORIENTATION[random_int(
            0,
            \count(PersonFeaturesList::ORIENTATION) - 1
        )];
        $adult->financialBracket = PersonFeaturesList::FINANCIAL_BRACKET[random_int(
            0,
            \count(PersonFeaturesList::FINANCIAL_BRACKET) - 1
        )];
        $adult->skinTone = PersonFeaturesList::SKIN_TONES[random_int(0, \count(PersonFeaturesList::SKIN_TONES) - 1)];
        $adult->foodPreference = PersonFeaturesList::FOOD_PREF[random_int(
            0,
            \count(PersonFeaturesList::FOOD_PREF) - 1
        )];
        $adult->eyeColor = PersonFeaturesList::EYE_COLOR[random_int(0, \count(PersonFeaturesList::EYE_COLOR) - 1)];
        $adult->hairColor = PersonFeaturesList::HAIR_COLOR[random_int(0, \count(PersonFeaturesList::HAIR_COLOR) - 1)];
        $adult->musicPreference = PersonFeaturesList::MUSIC_PREF[random_int(
            0,
            \count(PersonFeaturesList::MUSIC_PREF) - 1
        )];
        $adult->activityLevel = PersonFeaturesList::ACTIVITY_LEVEL[random_int(
            0,
            \count(PersonFeaturesList::ACTIVITY_LEVEL) - 1
        )];
        $adult->maritalStatus = PersonFeaturesList::MARITAL_STATUS[random_int(
            0,
            \count(PersonFeaturesList::MARITAL_STATUS) - 1
        )];
        $adult->age = random_int(20, 100);

        return $adult;
    }

    /**
     * @throws \Exception
     */
    private function createChild(): Child
    {
        $child = new Child();
        $child->firstName = PersonFeaturesList::FIRST_NAMES[random_int(0, \count(PersonFeaturesList::FIRST_NAMES) - 1)];
        $child->lastName = PersonFeaturesList::LAST_NAMES[random_int(0, \count(PersonFeaturesList::LAST_NAMES) - 1)];
        $child->gender = PersonFeaturesList::GENDERS[random_int(0, \count(PersonFeaturesList::GENDERS) - 1)];
        $child->sexualOrientation = PersonFeaturesList::ORIENTATION[random_int(
            0,
            \count(PersonFeaturesList::ORIENTATION) - 1
        )];
        $child->skinTone = PersonFeaturesList::SKIN_TONES[random_int(0, \count(PersonFeaturesList::SKIN_TONES) - 1)];
        $child->foodPreference = PersonFeaturesList::FOOD_PREF[random_int(
            0,
            \count(PersonFeaturesList::FOOD_PREF) - 1
        )];
        $child->eyeColor = PersonFeaturesList::EYE_COLOR[random_int(0, \count(PersonFeaturesList::EYE_COLOR) - 1)];
        $child->hairColor = PersonFeaturesList::HAIR_COLOR[random_int(0, \count(PersonFeaturesList::HAIR_COLOR) - 1)];
        $child->musicPreference = PersonFeaturesList::MUSIC_PREF[random_int(
            0,
            \count(PersonFeaturesList::MUSIC_PREF) - 1
        )];
        $child->activityLevel = PersonFeaturesList::ACTIVITY_LEVEL[random_int(
            0,
            \count(PersonFeaturesList::ACTIVITY_LEVEL) - 1
        )];
        $child->temperament = PersonFeaturesList::CHILD_BEHAVIOURS[random_int(
            0,
            \count(PersonFeaturesList::CHILD_BEHAVIOURS) - 1
        )];
        $child->styleOfAttachment = PersonFeaturesList::STYLES_OF_ATTACHMENT[random_int(
            0,
            \count(PersonFeaturesList::STYLES_OF_ATTACHMENT) - 1
        )];
        $child->age = random_int(0, 18);
        $child->height = random_int(50, 200);
        $child->weight = random_int(10, 120);

        return $child;
    }
}
