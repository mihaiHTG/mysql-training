<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
//        $user = new User();
//        $user->setEmail('admin@example.com');
//        $user->setRoles([User::ROLE_ADMIN]);
//        $user->setActive(true);
//        $manager->persist($user);
//
//        $user = new User();
//        $user->setEmail('user@example.com');
//        $user->setRoles([User::ROLE_USER]);
//        $user->setActive(true);
//        $manager->persist($user);
//
        $manager->flush();
    }
}
