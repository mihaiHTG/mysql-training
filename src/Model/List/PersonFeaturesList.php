<?php

declare(strict_types=1);

namespace App\Model\List;

class PersonFeaturesList
{
    public const FIRST_NAMES = [
        'Emma', 'Liam', 'Olivia', 'Noah', 'Ava', 'Isabella', 'Sophia', 'Mia', 'Charlotte', 'Amelia', 'Harper',
        'Evelyn', 'Abigail', 'Emily', 'Elizabeth', 'Sofia', 'Avery', 'Ella', 'Scarlett', 'Grace', 'Chloé', 'Victoria',
        'Madison', 'Avery', 'Chloe', 'Addison', 'Aubrey', 'Lily', 'Evelyn', 'Hazel', 'Avery', 'Penelope', 'Aria',
        'Riley', 'Brooklyn', 'Parker', 'Josiah', 'Serenity', 'Aaliyah', 'Samuel', 'Alexander', 'William', 'James',
        'Oliver', 'Benjamin', 'Elijah', 'Lucas', 'Mason', 'Logan', 'Michael', 'Daniel', 'Matthew', 'Samuel', 'Ethan',
        'Joseph', 'Alexander', 'Christopher', 'Andrew', 'Nicholas', 'Ryan', 'Tyler', 'Jacob', 'Nicholas', 'Alexander',
        'William', 'James', 'Alexander', 'Henry', 'Owen', 'Dylan', 'Caleb', 'Owen', 'Nathan', 'Gabriel', 'Ethan',
        'Isaac', 'Luke', 'Matthew', 'Ethan', 'Daniel', 'Nathan', 'Gabriel', 'Samuel', 'Benjamin', 'Nathan', 'Jack',
        'Ethan', 'Alexander', 'Michael', 'William', 'James', 'Matthew', 'Alexander', 'Henry', 'Owen', 'Dylan',
        'Caleb', 'Owen', 'Nathan', 'Gabriel', 'Ethan', 'Isaac', 'Luke', 'Matthew', 'Ethan', 'Daniel', 'Nathan',
        'Gabriel', 'Samuel', 'Benjamin', 'Nathan', 'Jack', 'Ethan', 'Alexander', 'Michael', 'William', 'James',
        'Alexander', 'Henry', 'Owen', 'Dylan', 'Caleb', 'Owen', 'Nathan', 'Gabriel', 'Ethan', 'Isaac', 'Luke',
        'Matthew', 'Ethan', 'Daniel', 'Nathan', 'Gabriel', 'Samuel', 'Benjamin', 'Nathan', 'Jack', 'Ethan',
        'Alexander', 'Michael', 'William', 'James', 'Alexander', 'Henry', 'Owen', 'Dylan', 'Caleb', 'Owen', 'Nathan',
        'Gabriel', 'Ethan', 'Isaac', 'Luke', 'Matthew', 'Ethan', 'Daniel', 'Nathan', 'Gabriel', 'Samuel', 'Benjamin',
        'Nathan', 'Jack', 'Ethan', 'Alexander', 'Michael', 'William', 'James', 'Alexander', 'Henry', 'Owen', 'Dylan',
    ];

    public const LAST_NAMES = [
        'Smith', 'Johnson', 'Williams', 'Jones', 'Brown', 'Davis', 'Garcia', 'Rodriguez', 'Martinez', 'Hernandez',
        'Lopez', 'Gonzalez', 'Wilson', 'Anderson', 'Thomas', 'Jackson', 'White', 'Harris', 'Martin', 'Thompson',
        'Young', 'Allen', 'King', 'Wright', 'Scott', 'Green', 'Baker', 'Adams', 'Nelson', 'Carter', 'Mitchell',
        'Perez', 'Roberts', 'Turner', 'Phillips', 'Campbell', 'Parker', 'Evans', 'Edwards', 'Coleman', 'Hudson',
        'Gibson', 'Cruz', 'Marshall', 'Ortiz', 'Gomez', 'Murray', 'Freeman', 'Wells', 'Webb', 'Simpson', 'Stevens',
        'Tucker', 'Porter', 'Hunter', 'Hicks', 'Crawford', 'Henry', 'Boyd', 'Mason', 'Morales', 'Kennedy', 'Warren',
        'Dixon', 'Ramirez', 'Torres', 'Peterson', 'Gray', 'Ramirez', 'James', 'Watson', 'Brooks', 'Kelly', 'Sanders',
        'Price', 'Bennett', 'Wood', 'Barnes', 'Ross', 'Henderson', 'Coleman', 'Coleman', 'Coleman', 'Alexander',
        'West', 'Jordan', 'Owens', 'Reynolds', 'Fisher', 'Ellis', 'Harrison', 'Gibson', 'Mcdonald', 'Hernandez',
        'Fernandez', 'Edwards', 'Turner', 'Phillips', 'Campbell', 'Parker', 'Evans', 'Edwards', 'Coleman', 'Hudson',
        'Gibson', 'Cruz', 'Marshall', 'Ortiz', 'Gomez', 'Murray', 'Freeman', 'Wells', 'Webb', 'Simpson', 'Stevens',
        'Tucker', 'Porter', 'Hunter', 'Hicks', 'Crawford', 'Henry', 'Boyd', 'Mason', 'Morales', 'Kennedy', 'Warren',
        'Dixon', 'Ramirez', 'Torres', 'Peterson', 'Gray', 'Ramirez', 'James', 'Watson', 'Brooks', 'Kelly', 'Sanders',
        'Price', 'Bennett', 'Wood', 'Barnes', 'Ross', 'Henderson', 'Coleman', 'Coleman', 'Coleman', 'Alexander',
        'West', 'Jordan', 'Owens', 'Reynolds', 'Fisher', 'Ellis', 'Harrison', 'Gibson', 'Mcdonald', 'Gonzalez',
        'Martinez', 'Lee', 'Hall', 'Allen', 'King', 'Wright', 'Scott', 'Green', 'Baker', 'Adams', 'Nelson', 'Carter',
        'Mitchell', 'Perez', 'Roberts', 'Turner', 'Phillips', 'Campbell', 'Parker', 'Evans', 'Edwards', 'Coleman',
        'Hudson', 'Gibson', 'Cruz', 'Marshall', 'Ortiz', 'Gomez', 'Murray', 'Freeman', 'Wells', 'Webb', 'Simpson',
        'Stevens', 'Tucker', 'Porter', 'Hunter', 'Hicks', 'Crawford', 'Henry', 'Boyd', 'Mason', 'Morales', 'Kennedy',
        'Warren', 'Dixon', 'Ramirez', 'Torres', 'Peterson', 'Gray', 'Ramirez', 'James', 'Watson', 'Brooks', 'Kelly',
        'Sanders', 'Price', 'Bennett', 'Wood', 'Barnes', 'Ross', 'Henderson', 'Coleman', 'Coleman', 'Coleman',
        'Alexander', 'West', 'Jordan', 'Owens', 'Reynolds', 'Fisher', 'Ellis', 'Harrison', 'Gibson', 'Mcdonald',
        'Clark', 'Lewis', 'Robinson', 'Walker', 'Perez', 'Hall', 'Young', 'Allen', 'King', 'Wright', 'Scott', 'Green',
        'Baker', 'Adams', 'Nelson', 'Carter', 'Mitchell', 'Perez', 'Roberts', 'Turner', 'Phillips', 'Campbell',
        'Parker', 'Evans', 'Edwards', 'Coleman', 'Hudson', 'Gibson', 'Cruz', 'Marshall', 'Ortiz', 'Gomez', 'Murray',
        'Freeman', 'Wells', 'Webb', 'Simpson', 'Stevens', 'Tucker', 'Porter', 'Hunter', 'Hicks', 'Crawford', 'Henry',
        'Boyd', 'Mason', 'Morales', 'Kennedy', 'Warren', 'Dixon', 'Ramirez', 'Torres', 'Peterson', 'Gray', 'Ramirez',
        'James', 'Watson', 'Brooks', 'Kelly', 'Sanders', 'Price', 'Bennett', 'Wood', 'Barnes', 'Ross', 'Henderson',
        'Coleman', 'Coleman', 'Coleman', 'Alexander', 'West', 'Jordan', 'Owens', 'Reynolds', 'Fisher', 'Ellis',
        'Harrison', 'Gibson', 'Mcdonald', 'Baker', 'Adams', 'Nelson', 'Carter', 'Mitchell', 'Perez', 'Roberts',
        'Turner', 'Phillips', 'Campbell', 'Parker', 'Evans', 'Edwards', 'Coleman', 'Hudson', 'Gibson', 'Cruz',
        'Marshall', 'Ortiz', 'Gomez', 'Murray', 'Freeman', 'Wells', 'Webb', 'Simpson', 'Stevens', 'Tucker', 'Porter',
        'Hunter', 'Hicks', 'Crawford', 'Henry', 'Boyd', 'Mason', 'Morales', 'Kennedy', 'Warren', 'Dixon', 'Ramirez',
        'Torres', 'Peterson', 'Gray', 'Ramirez', 'James', 'Watson', 'Brooks', 'Kelly', 'Sanders', 'Price', 'Bennett',
        'Wood', 'Barnes', 'Ross', 'Henderson', 'Coleman', 'Coleman', 'Coleman', 'Alexander', 'West',
    ];

    public const GENDERS = [
        'Male', 'Female', 'Non-binary/Genderqueer', 'Transgender', 'Intersex',
    ];

    public const ORIENTATION = [
        'Heterosexual', 'Homosexual', 'Bisexual', 'Asexual', 'Pansexual',
    ];

    public const FINANCIAL_BRACKET = [
        'Low Income', 'Lower-Middle Income', 'Middle Income', 'Upper-Middle Income', 'High Income',
    ];

    public const SKIN_TONES = [
        'Fair', 'Light', 'Medium', 'Tan', 'Olive', 'Dark', 'Ebony',
    ];

    public const FOOD_PREF = [
        'Vegetarian', 'Vegan', 'Pescatarian', 'Omnivore', 'Gluten-free', 'Dairy-free', 'Nut-free', 'Halal', 'Kosher',
        'Low-carb', 'High-protein',
    ];

    public const EYE_COLOR = [
        'Brown', 'Blue', 'Green', 'Hazel',
    ];

    public const HAIR_COLOR = [
        'Blonde', 'Brown', 'Black', 'Red', 'Auburn', 'Chestnut', 'Strawberry Blonde', 'Ginger', 'Gray', 'White',
        'Silver', 'Salt and Pepper',
    ];

    public const MUSIC_PREF = [
        'Pop', 'Rock', 'Hip-hop', 'Rap', 'Country', 'Blues', 'Jazz', 'Classical', 'Electronic', 'Metal', 'Soul', 'R&B',
        'Reggae', 'Folk', 'Funk', 'Latin', 'World Music',
    ];

    public const ACTIVITY_LEVEL = [
        'Sedentary', 'Low Active', 'Moderately Active', 'Active', 'Highly Active',
    ];

    public const MARITAL_STATUS = [
        'Single', 'Married', 'Divorced', 'Widowed', 'Separated', 'Domestic Partnership', 'Civil Union',
    ];

    public const CHILD_BEHAVIOURS = [
        'Active', 'Curious', 'Shy', 'Outgoing', 'Independent', 'Cooperative', 'Competitive', 'Confident', 'Timid',
        'Sensitive', 'Stubborn', 'Imaginative', 'Curious', 'Compassionate',
    ];

    public const STYLES_OF_ATTACHMENT = [
        'Secure', 'Anxious-Ambivalent', 'Anxious-Avoidant', 'Disorganized',
    ];

    public const HOBBIES = [
        'Reading | false', 'Writing | false', 'Photography | true', 'Travel | true', 'Drawing | true',
        'Painting | true', 'Cooking | true', 'Baking | true', 'Gardening | true', 'Hiking | false', 'Camping | true',
        'Fishing | true', 'Hunting | true', 'Yoga | true', 'Pilates | true', 'Running | false', 'Jogging | false',
        'Swimming | false', 'Biking | true', 'Skating | true', 'Skateboarding | true', 'Snowboarding | true',
        'Surfing | true', 'Rock Climbing | true', 'Weightlifting | true', 'Bodybuilding | true', 'Martial Arts | true',
        'Dance | true', 'Music | true', 'Singing | false', 'Playing an Instrument | true', 'Collecting | true',
        'Crafting | true', 'Knitting | false', 'Crochet | false', 'Sewing | false', 'Embroidery | false',
        'Quilting | false', 'Scrapbooking | false', 'Model Building | true', 'Video Gaming | true',
        'Board Gaming | true', 'Card Gaming | true', 'Role-Playing Gaming | true', 'Puzzle Solving | false',
        'Magic Tricks | true', 'Magic Collection | true', 'Chess | false', 'Checkers | false', 'Backgammon | false',
        'Bingo | false', 'Dominoes | false', 'Horseback Riding | true', 'Equestrian | true', 'Dog Training | true',
        'Cat Care | false', 'Fish Keeping | true', 'Bird Watching | false', 'Hiking | false',
        'Nature Photography | true', 'Stargazing | false', 'Astronomy | true', 'Rock Hunting | false',
        'Geocaching | false', 'Fossil Hunting | false', 'Archaeology | true', 'Antique Hunting | true',
        'Genealogy | true', 'History | false', 'Philosophy | false', 'Psychology | false', 'Economics | false',
        'Politics | false', 'Science | false', 'Technology | true', 'Mathematics | false', 'Programming | true',
        'Web Design | true', 'Graphic Design | true', 'Video Editing | true', 'Photography Editing | true',
        'Home Renovation | true', 'Woodworking | true', 'Automotive | true', 'Gardening | true', 'Home Cooking | true',
        'Gourmet Cooking | true', 'Baking | true', 'Wine Tasting | true', 'Whiskey Tasting | true', 'Craft Beer | true',
        'Microbrewing | true', 'Distilling | true', 'Gardening | true', 'Landscaping | true',
        'Interior Decorating | true', 'Exterior Decorating | true', 'Fashion | true', 'Makeup | true',
        'Skincare | true', 'Nail Care | true', 'Hair Care | true', 'Tanning | true', 'Massage | true', 'Spa | true',
        'Personal Training | true', 'Group Fitness | true', 'Yoga | true', 'Pilates | true', 'Swimming | false',
        'Jogging | false', 'Running | false', 'Biking | true', 'Skating | true', 'Skateboarding | true',
        'Snowboarding | true', 'Surfing | true', 'Rock Climbing | true', 'Weightlifting | true', 'Bodybuilding | true',
        'Martial Arts | true', 'Dance | true', 'Music | true', 'Singing | false', 'Playing an Instrument | true',
    ];

    public const PROFESSIONS = [
        'Teacher | 40000-90000', 'Nurse | 50000-100000', 'Engineer | 70000-150000', 'Doctor | 100000-300000',
        'Lawyer | 80000-200000', 'Accountant | 50000-120000', 'IT Professional | 60000-140000',
        'Marketing Professional | 50000-120000', 'Sales Professional | 40000-100000', 'HR Professional | 50000-120000',
        'Financial Analyst | 60000-140000', 'Project Manager | 70000-140000', 'Web Developer | 60000-120000',
        'Data Scientist | 80000-150000', 'Software Developer | 70000-150000', 'Business Analyst | 60000-120000',
        'Database Administrator | 60000-140000', 'Network Administrator | 60000-140000',
        'Systems Administrator | 60000-140000', 'Web Designer | 50000-100000', 'Graphic Designer | 50000-100000',
        'Content Creator | 40000-100000', 'Social Media Manager | 40000-100000', 'UX Designer | 60000-120000',
        'UI Designer | 60000-120000', 'Mobile Developer | 60000-140000', 'Game Developer | 60000-140000',
        'Data Entry Clerk | 30000-60000', 'Customer Service Representative | 30000-60000', 'Receptionist | 30000-50000',
        'Secretary | 30000-60000', 'Office Manager | 40000-80000', 'Executive Assistant | 40000-80000',
        'Bookkeeper | 40000-80000', 'Account Receivable Specialist | 40000-80000',
        'Account Payable Specialist | 40000-80000', 'Payroll Specialist | 40000-80000',
        'Logistics Coordinator | 40000-80000', 'Supply Chain Manager | 60000-120000',
        'Warehouse Manager | 50000-100000', 'Retail Manager | 40000-80000', 'Sales Manager | 60000-120000',
        'Marketing Manager | 60000-120000', 'HR Manager | 60000-120000', 'Operations Manager | 60000-120000',
        'IT Manager | 80000-150000', 'Project Manager | 70000-140000', 'Business Development Manager | 60000-120000',
        'Executive | 100000-300000', 'Entrepreneur | Variable', 'Consultant | 70000-200000',
        'Law Enforcement Officer | 50000-100000', 'Firefighter | 50000-100000',
        'Emergency Medical Technician | EMT) | 40000-70000', 'Paramedic | 50000-80000', 'Mechanic | 40000-80000',
        'Electrician | 40000-80000', 'Plumber | 40000-80000', 'Carpenter | 40000-80000', 'Painter | 40000-80000',
        'Roofing | 40000-80000', 'Chef | 40000-100000', 'Baker | 30000-60000', 'Waiter | 30000-60000',
        'Bartender | 30000-60000', 'Cleaner | 30000-60000', 'Janitor | 30000-60000', 'Home Health Aide | 30000-60000)',
    ];

    public const UNIVERSITIES = [
        'Harvard University', 'Princeton University', 'Massachusetts Institute of Technology (MIT)',
        'Stanford University', 'California Institute of Technology (Caltech)', 'Yale University',
        'Columbia University', 'University of Chicago', 'Duke University', 'John Hopkins University',
        'University of Pennsylvania', 'Northwestern University', 'Brown University', 'Cornell University',
        'Georgetown University', 'University of Notre Dame', 'University of California-Berkeley',
        'University of Michigan', 'University of Virginia', 'University of North Carolina at Chapel Hill',
        'University of Wisconsin-Madison', 'University of Texas at Austin', 'University of Minnesota',
        'University of Illinois at Urbana-Champaign', 'University of Maryland', 'University of Iowa',
        'University of Alabama', 'University of Washington', 'University of Oregon', 'University of Utah',
        'University of Oklahoma', 'University of Kansas', 'University of Arizona', 'University of New Mexico',
        'University of Nevada-Las Vegas', 'University of Colorado', 'University of Pittsburgh',
        'University of Rochester', 'University of Cincinnati', 'University of Louisville', 'University of Tennessee',
        'University of Georgia', 'University of South Carolina', 'University of Miami', 'University of Florida',
        'University of Central Florida', 'University of Missouri', 'University of Arkansas', 'University of Nebraska',
        'University of Kansas', 'University of Delaware', 'University of New Hampshire', 'University of Vermont',
        'University of Rhode Island', 'University of Maine', 'University of Massachusetts',
        'University of Connecticut', 'University of New York-Binghamton', 'University of New York-Stony Brook',
        'University of New York-Buffalo', 'University of New York-Albany', 'University of New York-SUNY',
        'University of New Jersey', 'University of Maryland-College Park', 'University of Virginia-Charlottesville',
        'University of North Carolina-Chapel Hill', 'University of Georgia-Athens',
        'University of Florida-Gainesville', 'University of Texas-Austin',
        'University of California-Los Angeles (UCLA)', 'University of California-San Diego (UCSD)',
        'University of California-Santa Barbara (UCSB)', 'University of California-Irvine (UCI)',
        'University of California-Riverside (UCR)', 'University of California-Santa Cruz (UCSC)',
        'University of California-Berkeley (UCB)', 'University of California-San Francisco (UCSF)',
        'University of Illinois-Urbana-Champaign', 'University of Michigan-Ann Arbor',
        'University of Wisconsin-Madison', 'University of Minnesota-Twin Cities', 'University of Colorado-Boulder',
        'University of Utah-Salt Lake City', 'University of Oregon-Eugene', 'University of Washington-Seattle',
        'University of Kansas-Lawrence', 'University of Oklahoma-Norman', 'University of Arizona-Tucson',
        'University of New Mexico-Albuquerque', 'University of Nevada-Las Vegas', 'University of Pittsburgh-Pittsburgh',
        'University of Cincinnati-Cincinnati', 'University of Louisville-Louisville',
        'University of Tennessee-Knoxville', 'University of Georgia-Athens', 'University of South Carolina-Columbia',
        'University of Miami-Coral Gables', 'University of Missouri-Columbia', 'University of Arkansas-Fayetteville',
        'University of Nebraska-Lincoln', 'University of Delaware-Newark', 'University of New Hampshire-Durham',
        'University of Vermont-Burlington', 'University of Rhode Island-Kingston', 'University of Maine-Orono',
        'University of Massachusetts-Amherst', 'University of Connecticut-Storrs', 'University of New York-Stony Brook',
        'University of New York-Binghamton', 'University of New York-Buffalo', 'University of New York-Albany',
        'University of New Jersey-New Brunswick', 'University of Virginia-Charlottesville',
        'University of North Carolina-Chapel Hill', 'University of Georgia-Athens', 'University of Florida-Gainesville',
        'University of Texas-Austin', 'University of California-Los Angeles (UCLA)', 'University',
    ];

    public const UNIVERSITY_LEVEL = [
        'Associate Degree', 'Bachelor Degree', 'Master Degree', 'Doctorate Degree', 'Professional Degree',
        'Postgraduate Diploma', 'Graduate Certificate', 'Higher National Diploma (HND)',
        'Higher National Certificate (HNC)', 'Executive MBA',
    ];

    public const SCHOOLS = [
        'Harvest Primary School', 'Harvard-Westlake School', 'Phillips Academy Andover', 'St. Pauls School',
        'Trinity School', 'Horace Mann School', 'Exeter Academy', 'Stuyvesant High School',
        'Bronx High School of Science', 'London Oratory School', 'Marlborough College', 'Eton College',
        'Kings College School', 'Winchester College', 'Dulwich College', 'University College School',
        'The Perse School', 'The Kings School', 'Canterbury', 'St. Albans School', 'The Leys School',
        'Tonbridge School', 'Repton School', 'Wellington College', 'Oundle School', 'The Portsmouth Grammar School',
        'Sherborne School', 'The Kings School', 'Chester', 'The Kings Edward VI School', 'Bath', 'The Dragon School',
        'Oxford', 'St. Marys School', 'Cambridge', 'St. Marys School', 'Ascot', 'The Royal Hospital School', 'Ipswich',
        'St. Olaves and St. Saviours Grammar School', 'Sevenoaks School', 'The John Lyon School', 'The Queens School',
        'Chester', 'The Haberdashers Ascot School', 'The Judd School', 'Tonbridge School', 'Christs Hospital',
        'Reading School', 'Mercers School', 'The Kings School', 'Rochester', 'St. John School', 'Leatherhead',
        'St. Edmund School', 'Canterbury',
    ];

    public const SCHOOL_LEVELS = [
        'Preschool', 'Kindergarten', 'Primary School', 'Middle School', 'High School', 'College', 'Vocational School',
    ];

    public const STREET_NAMES = [
        'Main Street', 'Park Avenue', 'Washington Avenue', 'Elm Street', 'Maple Street', 'Oak Street', 'Second Street',
        'Third Street', 'Fourth Street', 'Fifth Street', 'Sixth Street', 'Seventh Street', 'Eighth Street',
        'Ninth Street', 'Tenth Street', 'Eleventh Street', 'Twelfth Street', 'Thirteenth Street', 'Fourteenth Street',
        'Fifteenth Street', 'Sixteenth Street', 'Seventeenth Street', 'Eighteenth Street', 'Nineteenth Street',
        'Twentieth Street', 'Twenty-First Street', 'Twenty-Second Street', 'Twenty-Third Street',
        'Twenty-Fourth Street', 'Twenty-Fifth Street', 'Twenty-Sixth Street', 'Twenty-Seventh Street',
        'Twenty-Eighth Street', 'Twenty-Ninth Street', 'Thirtieth Street', 'Thirty-First Street',
        'Thirty-Second Street', 'Thirty-Third Street', 'Thirty-Fourth Street', 'Thirty-Fifth Street',
        'Thirty-Sixth Street', 'Thirty-Seventh Street', 'Thirty-Eighth Street', 'Thirty-Ninth Street',
        'Fortieth Street', 'Forty-First Street', 'Forty-Second Street', 'Forty-Third Street', 'Forty-Fourth Street',
        'Forty-Fifth Street', 'Forty-Sixth Street', 'Forty-Seventh Street', 'Forty-Eighth Street', 'Forty-Ninth Street',
        'Fiftieth Street', 'Fifty-First Street', 'Fifty-Second Street', 'Fifty-Third Street', 'Fifty-Fourth Street',
        'Fifty-Fifth Street', 'Fifty-Sixth Street', 'Fifty-Seventh Street', 'Fifty-Eighth Street', 'Fifty-Ninth Street',
        'Sixty-First Street', 'Sixty-Second Street', 'Sixty-Third Street', 'Sixty-Fourth Street', 'Sixty-Fifth Street',
        'Sixty-Sixth Street', 'Sixty-Seventh Street', 'Sixty-Eighth Street', 'Sixty-Ninth Street', 'Seventieth Street',
        'Seventy-First Street', 'Seventy-Second Street', 'Seventy-Third Street', 'Seventy-Fourth Street',
        'Seventy-Fifth Street', 'Seventy-Sixth Street', 'Seventy-Seventh Street', 'Seventy-Eighth Street',
        'Seventy-Ninth Street', 'Eightieth Street', 'Eighty-First Street', 'Eighty-Second Street',
        'Eighty-Third Street', 'Eighty-Fourth Street', 'Eighty-Fifth Street', 'Eighty-Sixth Street',
        'Eighty-Seventh Street', 'Eighty-Eighth Street', 'Eighty-Ninth Street', 'Ninetieth Street',
        'Ninety-First Street', 'Ninety-Second Street', 'Ninety-Third Street', 'Ninety-Fourth Street',
        'Ninety-Fifth Street', 'Ninety-Sixth Street', 'Ninety-Seventh Street', 'Ninety-Eighth Street',
        'Ninety-Ninth Street', 'One Hundredth Street', 'One Hundred and First Street', 'One Hundred and Second Street',
        'One Hundred and Third Street', 'One Hundred and Fourth Street', 'One Hundred and Fifth Street',
        'One Hundred and Sixth Street', 'One Hundred and Seventh Street', 'One Hundred and Eighth Street',
        'One Hundred and Ninth Street',
    ];

    public const CITY_STATE = [
        'New York, NY', 'Los Angeles, CA', 'Chicago, IL', 'Houston, TX', 'Phoenix, AZ', 'Philadelphia, PA',
        'San Antonio, TX', 'San Diego, CA', 'Dallas, TX', 'San Jose, CA', 'Austin, TX', 'Jacksonville, FL',
        'Fort Worth, TX', 'Columbus, OH', 'San Francisco, CA', 'Charlotte, NC', 'Indianapolis, IN', 'Seattle, WA',
        'Denver, CO', 'Washington, DC', 'Boston, MA', 'Nashville, TN', 'El Paso, TX', 'Detroit, MI', 'Memphis, TN',
        'Portland, OR', 'Oklahoma City, OK', 'Las Vegas, NV', 'Louisville, KY', 'Baltimore, MD', 'Milwaukee, WI',
        'Albuquerque, NM', 'Tucson, AZ', 'Fresno, CA', 'Sacramento, CA', 'Mesa, AZ', 'Atlanta, GA', 'Kansas City, MO',
        'Colorado Springs, CO', 'Miami, FL', 'Raleigh, NC', 'Omaha, NE',
    ];
}
