<?php

declare(strict_types=1);

namespace App\Model\List;

class DogBreedList
{
    public const BREED_LIST_UNEXPANDED = [
        'Labrador Retriever | Yellow, Black, Chocolate | Short',
        'German Shepherd | Black, Tan, Sable | Medium',
        'Bulldog | White, Brindle, Fawn | Short',
        'Golden Retriever | Golden | Medium',
        'French Bulldog | Fawn, Brindle, White | Short',
        'Poodle | White, Black, Brown, Apricot | Long',
        'Rottweiler | Black and Mahogany | Short',
        'German Shorthaired Pointer | Liver, Black, White | Short',
        'Siberian Husky | Black, White, Gray, Red | Medium',
        'Beagle | White, Brown, Black | Short',
        'Boxer | Fawn, Brindle, White | Short',
        'Dachshund | Black, Tan, Brown | Long or Short',
        'Pomeranian | Orange, Sable, Cream | Long',
        'Australian Shepherd | Black, Blue Merle, Red | Medium',
        'Yorkshire Terrier | Black, Tan | Long',
        'Shih Tzu | Black, White, Brown, Gold | Long',
        'Cocker Spaniel | Black, Brown, White | Medium',
        'Boston Terrier | Black, White | Short',
        'Great Dane | Black, Fawn, Blue, Harlequin | Short',
        'Saint Bernard | Red, Mahogany | Long',
        'Chihuahua | Black, Brown, White | Short',
        'Pug | Black, Fawn, Silver | Short',
        'Dalmatian | Black, White | Short',
        'Shetland Sheepdog | Black, Blue Merle, Sable | Long',
        'Weimaraner | Silver Gray, Blue Gray | Short',
        'English Springer Spaniel | Black, White, Liver | Medium',
        'Rhodesian Ridgeback | Wheaten | Short',
        'Bernese Mountain Dog | Black, White, Brown | Long',
        'Bichon Frise | White | Long',
        'Maltese | White | Long',
        'Akita | Red, White, Brindle | Short',
        'Cavalier King Charles Spaniel | Blenheim, Tricolor, Black and Tan | Long',
        'West Highland White Terrier | White | Long',
        'Lhasa Apso | Cream, Gold, White | Long',
        'English Cocker Spaniel | Black, Liver, Gold | Medium',
        'Schnauzer | Salt and Pepper, Black, White | Medium',
        'Irish Setter | Red | Long',
        'Golden Doodle | Apricot, Gold, Cream | Long',
        'Shiba Inu | Red, Black and Tan | Short',
        'Basset Hound | Black, White, Brown | Long',
        'American Eskimo Dog | White | Medium',
        'Chesapeake Bay Retriever | Brown, Sedona Red | Medium',
        'Great Pyrenees | White | Long',
        'Afghan Hound | Cream, Gold, Black | Long',
        'Samoyed | White | Long',
        'American Staffordshire Terrier | White, Black, Brown | Short',
        'English Setter | Blue Belton, Liver, Black | Long',
        'Australian Cattle Dog | Blue, Red | Short',
        'Icelandic Sheepdog | White, Black, Brown | Medium',
        'Keeshond | Silver and Black, Black and Silver | Medium',
        'Labradoodle | Cream, Apricot, Gold | Medium',
    ];

    public const DOG_NAME_LIST = [
        'Max',
        'Bella',
        'Charlie',
        'Daisy',
        'Luna',
        'Cooper',
        'Rocky',
        'Duke',
        'Sadie',
        'Daisy',
        'Molly',
        'Tucker',
        'Lola',
        'Rufus',
        'Bailey',
        'Bear',
        'Zeus',
        'Sadie',
        'Sophie',
        'Chloe',
        'Jackson',
        'Milo',
        'Marley',
        'Winston',
        'Oliver',
        'Penny',
        'Simba',
        'Lulu',
        'Rosie',
        'Diesel',
        'Bailey',
        'Zeus',
        'Olive',
        'Roxy',
        'Chloe',
        'Leo',
        'Lola',
        'Luna',
        'Daisy',
        'Bella',
        'Coco',
        'Rocky',
        'Duke',
        'Chloe',
        'Sadie',
        'Zeus',
        'Sadie',
        'Daisy',
        'Bailey',
        'Bear',
        'Marley',
        'Tucker',
        'Lola',
        'Rufus',
        'Milo',
        'Simba',
        'Sadie',
        'Sophie',
        'Chloe',
        'Zeus',
        'Olive',
        'Roxy',
        'Bailey',
        'Chloe',
        'Leo',
        'Lola',
        'Luna',
        'Daisy',
        'Bella',
        'Coco',
        'Rocky',
        'Duke',
    ];

    public const BEHAVIOURS = [
        'Social behavior', 'Communication', 'Play behavior', 'Hunting behavior', 'Predatory behavior',
        'Fearful behavior', 'Aggressive behavior', 'Attention-seeking behavior', 'Compulsive behavior',
        'Territorial behavior', 'Dominance behavior', 'Submission behavior', 'Resource guarding behavior',
        'Separation anxiety behavior', 'Destructive behavior', 'House training behavior',
        'Attention-motivated behavior', 'Eating behavior', 'Excitement behavior', 'Adaptive behavior',
        'Maternal behavior', 'Sexual behavior', 'Sleep behavior',
    ];
}
