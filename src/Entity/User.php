<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity(fields: ['email'])]
class User
{
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_USER = 'ROLE_USER';

    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    public Uuid $id;

    #[ORM\Column(type: Types::BOOLEAN)]
    public bool $active = false;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $email = '';

    #[ORM\Column(type: Types::STRING, length: 255)]
    public string $password = '';

    public string $plainPassword = '';

    /**
     * @var array<string>
     */
    #[ORM\Column(type: Types::JSON, nullable: false)]
    public array $roles = [];

    #[ORM\OneToOne(mappedBy: 'user', cascade: ['persist', 'remove'])]
    public ?Person $person = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    protected string $token = '';

    public function __construct()
    {
        $this->id = Uuid::v6();
    }
}
