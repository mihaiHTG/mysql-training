<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\AddressRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AddressRepository::class)]
#[ApiResource]
class Address
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    public Uuid $id;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $street = '';

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Assert\Positive]
    public int $number = 0;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $city = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $county = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $country = '';

    #[ORM\OneToOne(inversedBy: 'address', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    public ?Registration $registration = null;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'addresses')]
    #[ORM\JoinColumn(nullable: true)]
    public ?Adult $adult = null;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'addresses')]
    #[ORM\JoinColumn(nullable: true)]
    public ?Profession $profession = null;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }
}
