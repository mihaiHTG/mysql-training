<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ChildRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ChildRepository::class)]
#[ApiResource]
final class Child extends Person
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    public Uuid $id;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $temperament = '';

    #[ORM\Column(type: Types::BOOLEAN, nullable: false)]
    public bool $hasSiblings = false;

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Assert\Positive]
    public int $height = 0;

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Assert\Positive]
    public int $weight = 0;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $styleOfAttachment = '';

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'children')]
    #[ORM\JoinColumn(nullable: false)]
    public ?Adult $adult = null;

    /** @var ArrayCollection<int, Education> $educations */
    #[ORM\ManyToMany(targetEntity: Education::class, inversedBy: 'children', cascade: ['persist'])]
    public Collection $educations;

    public function __construct()
    {
        $this->id = Uuid::v6();
        $this->educations = new ArrayCollection();
    }
}
