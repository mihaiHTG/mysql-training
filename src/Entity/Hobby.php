<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\HobbyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: HobbyRepository::class)]
final class Hobby
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    public Uuid $id;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $name = '';

    #[ORM\Column(type: Types::BOOLEAN, nullable: false)]
    public bool $requiresInvesting = false;

    /** @var ArrayCollection<int, Adult> $adults */
    #[ORM\ManyToMany(targetEntity: Adult::class, mappedBy: 'hobbies', cascade: ['persist'])]
    public Collection $adults;

    public function __construct()
    {
        $this->id = Uuid::v6();
        $this->adults = new ArrayCollection();
    }
}
