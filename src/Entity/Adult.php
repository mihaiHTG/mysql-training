<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\AdultRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: AdultRepository::class)]
#[ApiResource]
final class Adult extends Person
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    public Uuid $id;

    /** @var ArrayCollection<int, Education> $educations */
    #[ORM\ManyToMany(targetEntity: Education::class, inversedBy: 'adults', cascade: ['persist'])]
    public Collection $educations;

    /** @var ArrayCollection<int, Hobby> $hobbies */
    #[ORM\ManyToMany(targetEntity: Hobby::class, inversedBy: 'adults', cascade: ['persist'])]
    public Collection $hobbies;

    /** @var ArrayCollection<int, Profession> $professions */
    #[ORM\ManyToMany(targetEntity: Profession::class, inversedBy: 'adults', cascade: ['persist'])]
    public Collection $professions;

    /** @var ArrayCollection<int, Child> $children */
    #[ORM\OneToMany(mappedBy: 'adult', targetEntity: Child::class, cascade: ['persist'], orphanRemoval: true)]
    public Collection $children;

    /** @var ArrayCollection<int, Dog> $dogs */
    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Dog::class, cascade: ['persist'], orphanRemoval: true)]
    public Collection $dogs;

    /** @var ArrayCollection<int, Address> $addresses */
    #[ORM\ManyToMany(targetEntity: Address::class, cascade: ['persist'], orphanRemoval: false)]
    public Collection $addresses;

    public function __construct()
    {
        $this->id = Uuid::v6();
        $this->educations = new ArrayCollection();
        $this->hobbies = new ArrayCollection();
        $this->professions = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->dogs = new ArrayCollection();
        $this->addresses = new ArrayCollection();
    }
}
