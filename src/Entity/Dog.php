<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\DogRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: DogRepository::class)]
#[ApiResource]
class Dog
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    public Uuid $id;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $firstName = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $lastName = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $behaviour = '';

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'dogs')]
    #[ORM\JoinColumn(nullable: false)]
    public ?Adult $owner = null;

    #[ORM\ManyToOne(cascade: ['persist'], inversedBy: 'dogs')]
    #[ORM\JoinColumn(nullable: false)]
    public ?Breed $breed = null;

    public function __construct()
    {
        $this->id = Uuid::v6();
    }
}
