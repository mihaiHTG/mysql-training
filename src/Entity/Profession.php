<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\ProfessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ProfessionRepository::class)]
class Profession
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    public Uuid $id;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $name = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public ?string $financialBracket = '';

    /** @var ArrayCollection<int, Address> $addresses */
    #[ORM\OneToMany(mappedBy: 'profession', targetEntity: Address::class, cascade: ['persist'], orphanRemoval: true)]
    public Collection $addresses;

    /** @var ArrayCollection<int, Adult> $adults */
    #[ORM\ManyToMany(targetEntity: Adult::class, mappedBy: 'professions', cascade: ['persist'])]
    public Collection $adults;

    public function __construct()
    {
        $this->id = Uuid::v6();
        $this->addresses = new ArrayCollection();
        $this->adults = new ArrayCollection();
    }
}
