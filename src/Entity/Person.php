<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\MappedSuperclass]
abstract class Person
{
    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $firstName = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $lastName = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $gender = '';

    #[ORM\OneToOne(inversedBy: 'person', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    public ?User $user = null;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $sexualOrientation = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $financialBracket = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $skinTone = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $foodPreference = '';

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    #[Assert\Positive]
    public int $age = 0;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $eyeColor = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $hairColor = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $musicPreference = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $activityLevel = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    public string $maritalStatus = '';
}
