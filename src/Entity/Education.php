<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\EducationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Uid\UuidV6;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: EducationRepository::class)]
class Education
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid', unique: true)]
    public UuidV6 $id;

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $name = '';

    #[ORM\Column(type: Types::STRING, length: 255, nullable: false)]
    #[Assert\NotBlank]
    public string $level = '';

    /** @var ArrayCollection<int, Adult> $adults */
    #[ORM\ManyToMany(targetEntity: Adult::class, mappedBy: 'educations', cascade: ['persist'])]
    public Collection $adults;

    /** @var ArrayCollection<int, Child> $children */
    #[ORM\ManyToMany(targetEntity: Child::class, mappedBy: 'educations', cascade: ['persist'])]
    public Collection $children;

    public function __construct()
    {
        $this->id = Uuid::v6();
        $this->adults = new ArrayCollection();
        $this->children = new ArrayCollection();
    }
}
