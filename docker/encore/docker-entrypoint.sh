#!/bin/sh

set -e

if [ -f '.env' ]; then
  . .env
fi
if [ -f '.env.local' ]; then
  . .env.local
fi

APP_ENV=${APP_ENV:-prod}
ENCORE_DEV_PORT=${ENCORE_DEV_PORT:-8080}

if [ "$APP_ENV" = 'dev' ]; then
    YARN_EXTRA_ARGS=" --pure-lockfile"
    # detect empty yarn.lock and create a valid one if needed
    YARN_LOCK_SIZE=$( stat -c %s yarn.lock )
    if [ "$YARN_LOCK_SIZE" = "0" ]; then
      YARN_EXTRA_ARGS=""
    fi
    yarn install "--no-progress --non-interactive$YARN_EXTRA_ARGS"
    yarn dev-server --host 0.0.0.0 --port "${ENCORE_DEV_PORT}" --public "${ENCORE_DEV_URL}"
fi

exec "$@"
