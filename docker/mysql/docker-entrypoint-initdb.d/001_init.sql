-- the main database creation is optional, can be handled by the `MYSQL_DATABASE` from docker-compose.yml
CREATE DATABASE IF NOT EXISTS `app`;
-- schema handled by doctrine:migrations
-- the test database creation is mandatory, otherwise test will fail - schema will be created automatically, but not the
-- database itself
CREATE DATABASE IF NOT EXISTS `app_test`