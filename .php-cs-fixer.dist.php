<?php

$finder = PhpCsFixer\Finder::create()
    ->in([
        __DIR__ . DIRECTORY_SEPARATOR . 'config',
        __DIR__ . DIRECTORY_SEPARATOR . 'src',
        __DIR__ . DIRECTORY_SEPARATOR . 'tests'
    ])
    ->exclude('var')
;

$config = new PhpCsFixer\Config();
return $config->setRules([
        // Preparing the upgrade path
        '@PHP80Migration' => true,
        '@PHP80Migration:risky' => true,
        // PhpCsFixer rules include the Symfony rules
        '@PhpCsFixer' => true,
        '@PhpCsFixer:risky' => true,
        '@DoctrineAnnotation' => true,
        'array_syntax' => ['syntax' => 'short'],
    ])
    ->setFinder($finder)
;
