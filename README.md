# Symfony, Api-Platform, Encore, Easy-Admin 

Symfony Web App

## Built with

We used Symfony framework and various other Symfony components for development.

* PHP 8.1
* MySQL (Percona Server) 8
* Symfony 5.4

More details about the libraries used in the `composer.json` file.

## Getting started

### Prerequisites

For development, you'll need to have installed:

* Docker
* docker-compose

### Running the project

1. Clone this repo
    1. Checkout (and pull/fetch) the `develop` branch for the latest version.
1. Start the docker containers using docker-compose:
    ```
    docker-compose up -d
    ```
    1. After changes made to the `Dockerfile` or other files related to the container setup and configuration
       you might need to rebuild the container images:
        ```
        docker-compose build && docker-compose up -d
        ```
1. Load fixtures into the database:
    ```
    docker-compose exec php bin/console doctrine:fixtures:load
    ```
1. Optional: To run commands inside the PHP container or for debugging use:
   ```
   docker-compose exec php sh
   # or
   docker-compose logs -f php
   ```

## Writing code

* Fork `main` to start working.
* Write the feature / bug fix code.
* In order to recompile assets (.js, .css) run
    ```
    docker-compose exec encore yarn run encore dev --watch
    ```
* Check the code quality using
    ```
    docker-compose exec php composer check-all
    ```
* Fix any issues reported by the suite of code quality tools setup in the project. Some degree of automated
  Coding Standard fixes can be done with PHP-CS-Fixer by running:
    ```
    docker-compose exec php composer cs-fix
    ```
* Write Unit and/or Functional **Tests** for important features or fixes!
  You can run the test easily:
    ```
    docker-compose exec php composer test
    ``` 
* Open pull requests against the `main` branch (make sure to pull the latest version).

### Code quality

While we started with the [Symfony coding standards](https://symfony.com/doc/5.3/contributing/code/standards.html), we
also highly recommend (and will later enforce) leveraging the PHP 8 type system and using strict types.

We use the following tools to ensure the quality of the code:
1. PHP CS Fixer - coding standard validator. It can also rewrite your code to make it compliant.
1. PHP CodeSniffer - another coding standard validator.
1. PHP Mess Detector - code quality evaluation like class sizes, complexity, unused code, etc.
1. PHPStan - static analysis of the code to help find problems even before writing tests + additional extensions (doctrine, phpunit, symfony, strict rules, banned code).
1. PHPUnit - for functional and unit tests


# References:
1. Xdebug 3 - Docker setup
   https://matthewsetter.com/setup-step-debugging-php-xdebug3-docker/
   https://dev.to/natterstefan/docker-tip-how-to-get-host-s-ip-address-inside-a-docker-container-5anh
   https://xdebug.org/docs/all_settings#discover_client_host
   https://www.jetbrains.com/help/phpstorm/configuring-xdebug.html#configuring-xdebug-docker
